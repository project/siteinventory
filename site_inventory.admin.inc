<?php

/**
 * @file
 * Administration page callbacks for Site Inventory.
 */

/**
 * Implements hook_admin_settings_form().
 */
function site_inventory_admin_settings_form($form_state) {
  drupal_add_js(drupal_get_path('module', 'site_inventory') . '/scripts/site_inventory_selectAll.js');
  $form['settings'] = array(
    '#type' => 'fieldset',
    '#title' => t('General settings'),
    '#collapsible' => FALSE,
  );

  $names = node_type_get_names();

  $form['settings']['site_inventory_node_types'] = array(
    '#type' => 'checkboxes',
    '#title' => t('Scan content types for site inventory') . ' (' . l(t('Select All'), '', array('attributes' => array('class' => 'selectAll'))) . ' | ' . l(t('Deselect All'), '', array('attributes' => array('class' => 'deselectAll'))) . ')',
    '#default_value' => variable_get('site_inventory_node_types', array()),
    '#options' => array_map('check_plain', $names),
    '#attributes' => array('class' => array('checkbox')),
  );

  $form['types'] = array(
    '#type' => 'fieldset',
    '#title' => t('Inventory Types'),
    '#collapsible' => FALSE,
  );

  $inventory_types = array(
    'document' => 'Documents',
    'media'    => 'Media',
    'email'    => 'Email Address',
    'phone number'    => 'Phone Numbers');

  $form['types']['site_inventory_types'] = array(
    '#type' => 'checkboxes',
    '#default_value' => variable_get('site_inventory_types', array()),
    '#title' => t('Choose type of items you would like to scan') . ' (' . l(t('Select All'), '', array('attributes' => array('class' => 'selectAll'))) . ' | ' . l(t('Deselect All'), '', array('attributes' => array('class' => 'deselectAll'))) . ')',
    '#options' => array_map('check_plain', $inventory_types),
    '#attributes' => array('class' => array('checkbox')),
  );

  $form['buttons']['submit'] = array('#type' => 'submit', '#value' => t('Save configuration'));
  return $form;
}

/**
 * Implements hook_admin_settings_form_submit().
 */
function site_inventory_admin_settings_form_submit($form, &$form_state) {
  // Save form settings.
  system_settings_form_submit($form, $form_state);

  $selected_node_types = array_filter(array_diff($form_state['values']['site_inventory_node_types'], $form['settings']['site_inventory_node_types']['#default_value']));
  $selected_inventory_types = array_filter(array_diff($form_state['values']['site_inventory_types'], $form['types']['site_inventory_types']['#default_value']));

  if (!empty($selected_node_types)) {
    if (!empty($selected_inventory_types)) {
      $nodes = _site_inventory_get_nodes($selected_node_types);
      module_load_include('inc', 'site_inventory', 'site_inventory.batch');
      batch_set(_site_inventory_batch_nodes($nodes, $selected_inventory_types));
    }
    else {
      $inventory_types = array_filter(variable_get('site_inventory_types', array()));
      if (!empty($inventory_types)) {
        $nodes = _site_inventory_get_nodes($selected_node_types);
        module_load_include('inc', 'site_inventory', 'site_inventory.batch');
        batch_set(_site_inventory_batch_nodes($nodes, $inventory_types));
      }
    }
  }
  elseif (!empty($selected_inventory_types)) {
    $node_types = array_filter(variable_get('site_inventory_node_types', array()));
    if (!empty($node_types)) {
      $nodes = _site_inventory_get_nodes($node_types);
      module_load_include('inc', 'site_inventory', 'site_inventory.batch');
      batch_set(_site_inventory_batch_nodes($nodes, $selected_inventory_types));
    }
  }

  // Delete inventory items from unselected content types.
  $delete_node_types = array_filter(array_diff($form['settings']['site_inventory_node_types']['#default_value'], $form_state['values']['site_inventory_node_types']));
  if (!empty($delete_node_types)) {
    $nodes = _site_inventory_get_nodes($delete_node_types);
    foreach ($nodes as $node) {
      _site_inventory_remove_node_from_database($node->nid);
    }
    _site_inventory_clean_database();
  }

  // Delete inventory items from unselected inventory types.
  $delete_inventory_types = array_filter(array_diff($form['types']['site_inventory_types']['#default_value'], $form_state['values']['site_inventory_types']));
  if (!empty($delete_inventory_types)) {
    _site_inventory_remove_type_from_database($delete_inventory_types);
  }
}
