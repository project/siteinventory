<?php
/**
 * @file
 * User page callbacks for the site inventory module.
 */

/**
 * Menu callback for general reporting.
 */
function site_inventory_admin_report_page() {
  global $base_url;
  $filter = site_inventory_build_filter_query();
  $rows = array();

  $build['site_inventory_filter_form'] = drupal_get_form('site_inventory_filter_form');

  $header = array(
    array('data' => t('Item'), 'field' => 'item', 'width' => '50%'),
    array('data' => t('Type'), 'field' => 'type', 'sort' => 'asc'),
    array('data' => t('Date Scanned'), 'field' => 'date_created'),
    array('data' => t('Operations')),
  );

  $query = db_select('site_inventory', 'si')->extend('PagerDefault')->extend('TableSort');
  $query->fields('si', array('siid', 'item', 'date_created', 'type'));
  if (!empty($filter['where'])) {
    $query->where($filter['where'], $filter['args']);
  }
  $result = $query
    ->limit(50)
    ->orderByHeader($header)
    ->execute();

  foreach ($result as $row) {
    $operations = array();
    switch ($row->type) {
      case 'phone number':
        $row->item = site_inventory_format_phone_number($row->item);
        break;

      case 'document':
      case 'media':
        if (url_is_external($row->item)) {
          $row->item = l(_filter_url_trim($row->item, 100), $row->item);
        }
        else {
          $row->item = l(_filter_url_trim($row->item, 100), $base_url . $row->item);
        }
        break;
    }
    $inventory_query = db_select('site_inventory_node', 'sin');
    $inventory_query->fields('sin', array('nid'));
    $inventory_query->condition('siid', $row->siid);
    $inventory_result = $inventory_query->execute();

    foreach ($inventory_result as $inventory_row) {
      $operations[] = l(t('Edit node') . ' ' . $inventory_row->nid, 'node/' . $inventory_row->nid . '/edit', array('query' => drupal_get_destination()));
    }

    $rows[] = array(
      'data' => array(
        $row->item,
        ucwords($row->type),
        format_date($row->date_created, 'short'),
        theme('item_list', array('items' => $operations)),
      ),
    );
  }

  // Prepare table rows for inventory type count.
  $types = _site_inventory_get_type_extensions();
  $types = array_keys($types);
  $rows_count = array();
  $query_count_total = 0;

  foreach ($types as $type) {
    $query_count = db_select('site_inventory', 'si')
      ->fields('si', array('siid'))
      ->condition('type', $type)
      ->execute()
      ->rowCount();

    if ($query_count > 0) {
      $rows_count[] = array(
        'data' => array(
          ucwords($type),
          $query_count,
        ),
      );
    }
    $query_count_total += $query_count;
  }

  $rows_count[] = array(
    'data' => array(
      'Total',
      $query_count_total,
    ),
  );

  if ($query_count_total > 0) {
    $header_count = array(
      array('data' => t('Type'), 'width' => '15%'),
      array('data' => t('Count')),
    );

    $build['site_inventory_table_count'] = array(
      '#theme' => 'table',
      '#header' => $header_count,
      '#rows' => $rows_count,
    );
  }

  $build['site_inventory_table'] = array(
    '#theme' => 'table',
    '#header' => $header,
    '#rows' => $rows,
    '#empty' => t('No items available, please configure') . ' ' . l(t('your settings'), 'admin/config/content/site_inventory') . '.',
  );

  $build['site_inventory_pager'] = array('#theme' => 'pager');
  return $build;
}

/**
 * Form constructor for the database logging filter form.
 *
 * @ingroup forms
 */
function site_inventory_filter_form($form) {
  $filters = site_inventory_filters();

  $form['filters'] = array(
    '#type' => 'fieldset',
    '#title' => t('Filter inventory items'),
    '#collapsible' => TRUE,
    '#collapsed' => empty($_SESSION['site_inventory_overview_filter']),
  );
  $form['filters']['search'] = array(
    '#type' => 'textfield',
    '#title' => t('Search'),
    '#default_value' => (isset($_SESSION['site_inventory_overview_filter']['search'])) ? $_SESSION['site_inventory_overview_filter']['search'] : '',
  );
  foreach ($filters as $key => $filter) {
    $form['filters']['status'][$key] = array(
      '#title' => check_plain($filter['title']),
      '#type' => 'select',
      '#multiple' => TRUE,
      '#size' => 5,
      '#options' => $filter['options'],
    );
    if (!empty($_SESSION['site_inventory_overview_filter'][$key])) {
      $form['filters']['status'][$key]['#default_value'] = $_SESSION['site_inventory_overview_filter'][$key];
    }
  }

  $form['filters']['actions'] = array(
    '#type' => 'actions',
    '#attributes' => array('class' => array('container-inline')),
  );

  $form['filters']['actions']['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Filter'),
  );
  if (!empty($_SESSION['site_inventory_overview_filter'])) {
    $form['filters']['actions']['reset'] = array(
      '#type' => 'submit',
      '#value' => t('Reset'),
    );
  }
  return $form;
}

/**
 * Form validation handler for site_inventory_filter_form().
 *
 * @see site_inventory_filter_form_submit()
 */
function site_inventory_filter_form_submit($form, &$form_state) {
  $op = $form_state['values']['op'];
  $filters = site_inventory_filters();
  switch ($op) {
    case t('Filter'):
      foreach ($filters as $name => $filter) {
        if (isset($form_state['values'][$name])) {
          $_SESSION['site_inventory_overview_filter'][$name] = $form_state['values'][$name];
        }
        if (isset($form_state['values']['search'])) {
          $_SESSION['site_inventory_overview_filter']['search'] = $form_state['values']['search'];
        }
      }
      break;

    case t('Reset'):
      $_SESSION['site_inventory_overview_filter'] = array();
      break;

  }
  return 'admin/reports/site_inventory';
}

/**
 * Builds a query for site inventory administration filters based on session.
 *
 * @return array
 *   An associative array with keys 'where' and 'args'.
 */
function site_inventory_build_filter_query() {
  if (empty($_SESSION['site_inventory_overview_filter'])) {
    return array();
  }

  $filters = site_inventory_filters();

  $where = $args = array();

  if (!empty($_SESSION['site_inventory_overview_filter']['search'])) {
    $where[] = 'si.item LIKE ?';
    $args[] = '%' . $_SESSION['site_inventory_overview_filter']['search'] . '%';
  }

  foreach ($_SESSION['site_inventory_overview_filter'] as $key => $filter) {
    $filter_where = array();
    foreach ($filter as $value) {
      $filter_where[] = $filters[$key]['where'];
      $args[] = $value;
    }
    if (!empty($filter_where)) {
      $where[] = '(' . implode(' OR ', $filter_where) . ')';
    }
    $where = !empty($where) ? implode(' AND ', $where) : '';

    return array(
      'where' => $where,
      'args' => $args,
    );
  }
}

/**
 * Creates a list of site inventory administration filters that can be applied.
 *
 * @return array
 *   Associative array of filters. The top-level keys are used as the form
 *   element names for the filters, and the values are arrays with the following
 *   elements:
 *   - title: Title of the filter.
 *   - where: The filter condition.
 *   - options: Array of options for the select list for the filter.
 */
function site_inventory_filters() {
  $filters = array();

  foreach (_site_inventory_get_types() as $type) {
    $types[$type] = ucwords($type);
  }

  if (!empty($types)) {
    $filters['type'] = array(
      'title' => t('Type'),
      'where' => "si.type = ?",
      'options' => $types,
    );
  }

  return $filters;
}

/**
 * Formats phone numbers for readability.
 *
 * @param string $number
 *   The non-formatted number.
 *
 * @return string
 *   A human-readable number format.
 */
function site_inventory_format_phone_number($number) {
  $offset = (strlen($number) > 10) ? 1 : 0;
  return substr($number, $offset, 3) . '-' . substr($number, $offset + 3, 3) . '-' . substr($number, $offset + 6);
}
