
CONTENTS OF THIS FILE
---------------------

 * Introduction
 * Installation
 * Configuration
 * Reporting
 * Maintainers


INTRODUCTION
------------

Site Inventory is a module that stores a repository of key content on your site 
including:

 * Email Addresses
 * Phone Numbers
 * Documents
 * Media Files

The goal of this module is to help users track and search nodes for key content 
that may need to be updated or removed. This module assists content managers 
with content inventory and content audit.


INSTALLATION
------------

 * Install as you would normally install a contributed Drupal module.


CONFIGURATION
-------------

 * Customize the node types you would like to inventory in
   Configuration » Content authoring » Site Inventory


REPORTING
---------

 * Go to Reports » Site Inventory to view your inventory
 * You can filter your results using Search and Type fields


MAINTAINERS
-----------

Current Maintainers:
 * Alex Ho (mralexho) - https://www.drupal.org/user/800278
