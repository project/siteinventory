/**
 * @file
 * Provides click handlers for selecting/deselecting checkboxes.
 */

(function ($) {

  Drupal.behaviors.selectAll = {
    attach: function (context, settings) {
      $('.selectAll', context).click(function (e) {
        e.preventDefault();
        $(this)
          .closest('.form-type-checkboxes')
          .find('.checkbox')
          .attr('checked', 'checked');
      });
      $('.deselectAll', context).click(function (e) {
        e.preventDefault();
        $(this)
          .closest('.form-type-checkboxes')
          .find('.checkbox')
          .removeAttr('checked');
      });
    }
  }

})(jQuery);
