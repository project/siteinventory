<?php

/**
 * @file
 * Batch API callbacks for the Site Inventory module.
 */

/**
 * Batch nodes associated with inventory types.
 *
 * @param array $nodes
 *   Array of nodes.
 * @param array $inventory_types
 *   Array of inventory item types.
 *
 * @return array
 *   Return batch operations.
 */
function _site_inventory_batch_nodes($nodes = array(), $inventory_types = array()) {
  foreach ($nodes as $node) {
    $operations[] = array(
      '_site_inventory_batch_import_op', array($node->nid, $inventory_types));
  }

  $batch = array(
    'file' => drupal_get_path('module', 'site_inventory') . '/site_inventory.batch.inc',
    'title' => t('Site Inventory batch'),
    'operations' => $operations,
    'init_message' => t('Site Inventory initializing'),
    'error_message' => t('Site Inventory encountered an error.'),
    'finished' => '_site_inventory_batch_finished',
  );

  return $batch;
}

/**
 * Batch operation: Import one by one node for inventory.
 *
 * @param int $nid
 *   The Node ID.
 * @param array $inventory_types
 *   Array of inventory item types.
 * @param array $context
 *   Batch context array.
 */
function _site_inventory_batch_import_op($nid, $inventory_types = array(), &$context = array()) {
  $node = node_load($nid);
  _site_inventory_extract_contents($node, $inventory_types);

  // Store results for post-processing in the finished callback.
  $context['results'][] = $node->nid;
  $context['message'] = t('Content: @title', array('@title' => $node->title));
}

/**
 * Output node batch result messages.
 *
 * @param bool $success
 *   If inventory completed successfully or not.
 * @param int $results
 *   Number of nodes inventoried.
 * @param array $operations
 *   Array of functions called.
 */
function _site_inventory_batch_finished($success, $results, $operations = array()) {
  if ($success) {
    $message = format_plural(count($results), 'One node has been scanned.', '@count nodes have been scanned.');
  }
  else {
    $message = t('Scanning for links in content has failed with an error.');
  }
  drupal_set_message($message);
}
